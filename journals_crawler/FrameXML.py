from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom.minidom import parse, parseString


class XMLCreator(object):

    def __init__(self, frame_id=None, frame_name=None):
        self.root = Element('frame')
        if frame_id:
            self.root.attrib['ID'] = str(frame_id)
        if frame_name:
            self.root.attrib['name'] = frame_name
        self.root.append(Element('definition'))
        self.root.append(Element('fes'))
        self.root.append(Element('lexunits'))

    @property
    def frame_id(self):
        return self.root.attrib['ID']

    @frame_id.setter
    def frame_id(self, value):
        self.root.attrib['ID'] = str(value)

    @property
    def frame_name(self):
        return self.root.attrib['name']

    @frame_name.setter
    def frame_name(self, value):
        self.root.attrib['name'] = value

    @property
    def definition(self):
        return self.root.find('definition') or None

    @definition.setter
    def definition(self, value):
        if self.root.find('definition') is None:
            temp = Element('definition')
            temp.text = str(value)
            self.root.append(temp)
        else:
            self.root.find('definition').text = str(value)

    def add_fe(self, fe_id, fe_name=None, fe_coretype=None, definition=None, semtypes=None):
        temp = Element('fe')
        temp.attrib['ID'] = str(fe_id)
        if fe_name:
            temp.attrib['name'] = str(fe_name)
        if fe_coretype:
            temp.attrib['coretype'] = str(fe_coretype)

        def_elem = Element('definition')
        if definition:
            def_elem.text = definition
        temp.append(def_elem)

        semtypes_elem = Element('semtypes')
        if semtypes is not None:
            for t in semtypes:
                type_temp = Element('semtype')
                type_temp.text = str(t)
                semtypes_elem.append(type_temp)
        temp.append(semtypes_elem)

        self.root.find('fes').append(temp)

    def remove_fe(self, id):
        for t in self.root.findall(".//fe[@ID='"+str(id)+"']"):
            self.root.find('fes').remove(t)


    def add_lexunit(self, lu_id, lu_name=None, lu_pos=None, definition=None, semtypes=None, lexem_id=None, lexem_name = None, lexem_pos=None, annotated=0, total=0):
        temp = Element('lexunit')
        temp.attrib['ID'] = str(lu_id)
        if lu_name:
            temp.attrib['name'] = str(lu_name)
        if lu_pos:
            temp.attrib['pos'] = str(lu_pos)

        def_elem = Element('definition')
        if definition:
            def_elem.text = definition
        temp.append(def_elem)

        semtypes_elem = Element('semtypes')
        if semtypes is not None:
            for t in semtypes:
                type_temp = Element('semtype')
                type_temp.text = str(t)
                semtypes_elem.append(type_temp)
        temp.append(semtypes_elem)

        annotation_elem = Element('annotation')
        annotation_elem.append(Element('annotated'))
        annotation_elem.append(Element('total'))
        annotation_elem.find('annotated').text = str(annotated)
        annotation_elem.find('total').text = str(total)
        temp.append(annotation_elem)


        lexem_elem = Element('lexeme')
        if lexem_id:
            lexem_elem.set('ID', str(lexem_id))
        if lexem_name:
            lexem_elem.set('name', str(lexem_name))
        if lexem_pos:
            lexem_elem.set('pos', str(lexem_pos))
        temp.append(lexem_elem)


        self.root.find('lexunits').append(temp)


    def remove_lu(self, id):
        for t in self.root.findall(".//lexunits[@ID='"+str(id)+"']"):
            self.root.find('lexunits').remove(t)


    def __str__(self):
        xml = parseString(tostring(self.root))
        pretty_xml_as_string = xml.toprettyxml()
        return pretty_xml_as_string

