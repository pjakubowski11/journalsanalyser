import json
import os
import scrapy
from scrapy.signalmanager import SignalManager
from scrapy.xlib.pydispatch import dispatcher
import urllib2
from journals_crawler.items import MLArticle
from journals_crawler.utils import remove_tags
import sqlite3


class Sprider1(scrapy.Spider):
    name = "sprider1"
    allowed_domains = ["dblp.uni-trier.de", "link.springer.com"]
    #start_urls = [("http://dblp.uni-trier.de/db/journals/ml/ml" + str(i) + ".html") for i in range(101)]
    start_urls = ["http://dblp.uni-trier.de/db/journals/ml/ml90.html", ]
    db_name = 'articles.db'
    conn = None
    lastFileNo = 0

    def __init__(self):
        self.conn = sqlite3.connect(self.db_name)
        self.iterator = 0
        for i in os.listdir("downloaded/open_access"):
            a = [j for j in i if j.isdigit()]
            if len(a) and int(''.join(a)) > self.lastFileNo:
                self.lastFileNo = int(''.join(a))
        super(Sprider1, self).__init__()

    def parse(self, response):

        for j in response.selector.xpath('//li[@class="entry article"]'):
            if len(j.xpath('nav[@class="publ"]//a[@itemprop="url"]/@href')):
                url = j.xpath('nav[@class="publ"]//a[@itemprop="url"]/@href')[0].extract()
                url = urllib2.urlopen(url).geturl()
                yield scrapy.Request(str(url) + '/fulltext.html', callback=self.parse_article)

    def parse_article(self, response):
        item = MLArticle()
        item['title'] = remove_tags(response.selector.css('.ArticleTitle')[0].extract()).strip() or ""
        item['pub_time'] = remove_tags(response.selector.css('.ArticleCitation_Year time')[0].extract()).strip() or ""

        item['volume'] = remove_tags(response.selector.css('.ArticleCitation_Volume')[0].extract())[0:-1].strip() or ""
        item['issue'] = remove_tags(response.selector.css('.ArticleCitation_Issue')[0].extract())[0:-1].strip() or ""
        item['content'] = ""
        item['authors'] = []
        item['references'] = []

        # authors
        for j in response.selector.css('.AuthorName'):
            item['authors'].append(remove_tags(j.extract()))

        # article text
        for p in response.selector.css('p.Para'):
            item['content'] += remove_tags(p.extract())

        # references
        for b in response.selector.css('.CitationContent'):
            item['references'].append(remove_tags(b.extract()))

        f = open('downloaded/open_access/ml' + str(self.iterator + self.lastFileNo) + ".txt", 'w+')
        f.write(item['content'].encode('utf-8'))
        f.close()
        filepath = 'downloaded/open_access/ml' + str(self.iterator) + '.txt'
        print filepath
        self.iterator += 1

        self.conn.execute("INSERT INTO SPRINGER (TITLE, PUB_TIME, VOLUME, ISSUE, AUTHORS, REFS, PATH) \
                       VALUES ('" + item['title'] + "', '" + item['pub_time'] + "', '" + item['volume'] + "', '" +
                      item['issue'] + "', '" + json.dumps(item['authors']) + "', '" + json.dumps(
        item['references']) + "', '" + filepath + "');")




    def close(self, reason):
        self.conn.commit()
        self.conn.close()
