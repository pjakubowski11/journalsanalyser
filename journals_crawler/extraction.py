import os

import errno
import nltk
import signal
from nltk import sent_tokenize
from functools import wraps


class SentenceFinder(object):

    word_list = []
    text_str = ""
    sentences_tab = []
    active_word_t = ""

    def __init__(self, filename, type, source_directory):
        with open(filename) as f:
            for temp in f:
                if type == 1:  # simple word
                    self.word_list.append(eval(temp)[0])
                elif type == 2:  # bigram
                    self.word_list.append(eval(temp)[0][0] + " " + eval(temp)[0][1])
        for fname in os.listdir(source_directory):
            self.text_str += open(source_directory + '/'+fname).read()

        self.sentences_tab = sent_tokenize(unicode(self.text_str, errors='ignore'))

    @property
    def words(self):
        return self.word_list

    @property
    def sentences(self):
        return self.sentences_tab

    @property
    def active_word(self):
        return self.active_word_t

    def find_sentences(self, word):
        self.active_word_t = word
        for s in self.sentences:
            if word in s:
                yield s


class TimeoutError(Exception):
    def __init__(self, sentence):
        super(TimeoutError, self).__init__()
        self.message = "Timeout: " + str(sentence)


class timeout:
    def __init__(self, seconds=1, error_message='Timeout', sentence=""):
        self.seconds = seconds
        self.error_message = error_message
        self.sentence = sentence

    def handle_timeout(self, signum, frame):
        raise TimeoutError(sentence=self.sentence)

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)

    def __exit__(self, type, value, traceback):
        signal.alarm(0)


def getNodes(parent, good, active_word):
    res_tab = []
    next_level = []
    for node in parent:
        if type(node) is nltk.Tree:
            if node.label() == 'NP' and good == 0:
                good += 1
                res_tab.append(node.leaves())
            elif node.label().startswith("V") and good == 1:
                res_tab.append(node.leaves())
                good += 1

            next_level.append(node)
            aw_tab = active_word.split(' ')
            if good == 2:
                for a in aw_tab:
                    if any(a in s for s in res_tab):
                        return res_tab

    good = 0

    for n in next_level:
        res = getNodes(n, good, active_word)
        if res:
            return res
    return False
