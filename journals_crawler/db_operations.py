import sqlite3


def recreate_table(dbname):
    conn = sqlite3.connect(dbname)
    conn.execute("DROP TABLE SPRINGER")
    conn.execute('''CREATE TABLE IF NOT EXISTS SPRINGER
       (TITLE   TEXT    NOT NULL,
       PUB_TIME TEXT,
       VOLUME   TEXT,
       ISSUE    TEXT,
       AUTHORS  TEXT,
       REFS     TEXT,
       PATH         CHAR(50));''')
    conn.close()


def print_table(dbname, tablename):
    conn = sqlite3.connect(dbname)
    cur = conn.cursor()
    cur.execute("SELECT * FROM "+tablename+" ;")
    for j in cur.fetchall():
        print j
    conn.close()