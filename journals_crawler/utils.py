import re

import nltk
from nltk import RegexpParser
from nltk import sent_tokenize, word_tokenize, pos_tag
from nltk.corpus import stopwords
from nltk.stem.snowball import PorterStemmer


def remove_tags(text):
    if text is None:
        return text
    text = re.sub("<[/]*br[/]*>", "\n", text)
    text = re.sub("<.*?[^>]>", "", text)
    return re.sub('\'', '', text)


def remove_stopwords(words_tab):
    return [word for word in words_tab if word not in stopwords.words('english')]


def lematize(text):
    sentences = sent_tokenize(text)
    sentences = [word_tokenize(sent) for sent in sentences]
    sentences = [pos_tag(sent) for sent in sentences]

    return sentences


def chunk(sentence):
    """
    NVN = rzeczownik czasownik rzeczownik - ALA MA KOTA
    :param sentence:

    """
    print sentence
    grammar = "NVN: {<PR.*>?<DT>?<JJ.*|JJ>*<NN.*>+<CD>*<WDT>*<RB>*<VB.*>+<DT>?<JJ.*>*<NN.*>+}"
    cp = RegexpParser(grammar)
    cp.parse(sentence).draw()


def morphy_tag(penn_tag):
    wnpos = lambda e: ('a' if e[0].lower() == 'j' else e[0].lower()) if e[0].lower() in ['n', 'r', 'v'] else 'n'
    return wnpos(penn_tag)


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = []
    for item in tokens:
        stems.append(PorterStemmer().stem(item))
    return stems


class StopwordsExtension(object):

    ext_words = [
        '',
    ]
    stopwords_tab = stopwords.words("english") + ext_words

    @classmethod
    def get_stopwords(cls):
        return cls.stopwords_tab
