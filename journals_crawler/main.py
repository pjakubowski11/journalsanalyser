import os
import time
import urllib2
import sqlite3
import logging
import datetime

import nltk
from stat_parser import Parser
from journals_crawler.db_operations import recreate_table
from journals_crawler.extraction import SentenceFinder, getNodes, timeout
from journals_crawler.stat_preprocessing import StatPreProcessor
from journals_crawler.stats import frequency_words, remove_stopwords, tfidf_frequency, bigram_frequency
from preprocessing import PreProcessor
from FrameXML import XMLCreator

if __name__ == "__main__":

    logging.basicConfig(filename='janalyser.log', level=logging.INFO)
    logging.info("Start processing: " + str(datetime.datetime.today()))
    #recreate_table('articles.db')
    #os.system("scrapy crawl sprider1")
    #p = PreProcessor('articles.db')
    #c = StatPreProcessor('articles.db')
    #for p in c.parsed_sentences('springer'):
    #    print p
    #p.process('springer')
    iterator = 0

    """
    a = XMLCreator(34, 'ramka1')
    a.definition = "lala"
    a.definition = "tata"
    a.add_fe(23, "cos", "type", "definicja", ["czlowiek", "roslina"])
    #a.remove_fe(23)

    a.add_lexunit(45, "drive", "V", "definnn", ["ttyp1"], 1, "lexem1", "N", 10, 9)
    #a.remove_lu(45)
    print a
    """
    # ramka algorithm

    a = XMLCreator(1, 'algorithm')
    a.definition = "This is a frame for representing ML Algorithms and their properties."
    a.add_fe(1, "Algorithm", "True", "The ML Algorithm.", ["dmop:DM-Algorithm", ])
    a.add_fe(2, "Instance", "False", "This FE identifies the particular Instance of the ML Algorithm (specific algorithm name).", ["dmop:DM-Algorithm", ])
    a.add_fe(3, "Task", "False", "This FE identifies the ML Task addressed by the ML Algorithm.", ["dmop:DM-Task", ])
    a.add_fe(4, "Data", "False", "This FE identifies Data  the ML Algorithm specifies on input.", ["dmop:DM-Data", ])
    a.add_fe(5, "Hypothesis", "False", "This FE identifies the ML Hypothesis the ML Algorithm produces.", ["dmop:DM-Hypothesis", ])
    a.add_fe(6, "Software", "False", "This FE identifies ML Software (environment) where the ML Algorithm is implemented.", ["dmop:DM-Software", ])
    a.add_fe(7, "OptimizationProblem", "False", "This FE indentifies the OptimizationProblem the ML Algorithm is trying to solve.", ["dmop:OptimizationProblem", ])

    a.add_lexunit(1, "algorithm", "N")
    a.add_lexunit(2, "learning algorithm", "N")
    a.add_lexunit(3, "method", "N")
    a.add_lexunit(4, "call", "V")
    a.add_lexunit(5, "name", "V")
    a.add_lexunit(6, "extend", "V")
    a.add_lexunit(7, "extension", "V")
    a.add_lexunit(8, "enhance", "V")
    a.add_lexunit(9, "generate", "V")
    a.add_lexunit(10, "learn", "V")
    a.add_lexunit(11, "train", "V")
    a.add_lexunit(12, "implement", "V")

    print a

    # ramka data

    a = XMLCreator(1, 'data')
    a.definition = "This frame represents Data, the Quantity or dimensions  associated with given data (e.g, a number of datasets, number of features), identifies the Origin of data, its Characteristic, its Name  (e.g., of a particular dataset)."
    a.add_fe(1, "Data", "True", "Various levels of granularity of Data: a dataset , a data table, a feature, an instance, a feature-value.", ["dmop:DM-Data", ])
    a.add_fe(2, "Quantity", "False", "This FE identifies the Quantity or dimensions associated with given Data (e.g, a number of datasets, number of features).", [])
    a.add_fe(3, "Origin", "False", "This FE identifies Origin of Data. ", [])
    a.add_fe(4, "Characteristic", "False", "This FE identifies the Characteristic of the Data. ", ["dmop:DataCharacteristic", ])
    a.add_fe(5, "Name", "False", "This FE identifies the Name of Data (of a particular dataset).", [])

    a.add_lexunit(1, "data", "N")
    a.add_lexunit(2, "data set", "N")
    a.add_lexunit(3, "training set", "N")
    a.add_lexunit(4, "training data", "N")
    a.add_lexunit(5, "training examples", "N")
    a.add_lexunit(6, "examples", "N")
    a.add_lexunit(7, "data point", "N")
    a.add_lexunit(8, "test set", "N")
    a.add_lexunit(9, "label ranking", "N")
    a.add_lexunit(10, "preference information", "N")
    a.add_lexunit(11, "background knowledge", "N")
    a.add_lexunit(12, "prior knowledge", "N")
    a.add_lexunit(13, "missing values", "N")
    a.add_lexunit(14, "ground truth", "N")
    a.add_lexunit(15, "unlabeled data", "N")
    a.add_lexunit(16, "data stream", "N")
    a.add_lexunit(17, "positive examples", "N")
    a.add_lexunit(19, "class labels", "N")
    a.add_lexunit(20, "real data", "N")
    a.add_lexunit(21, "gene expression", "N")
    a.add_lexunit(22, "missing data", "N")
    a.add_lexunit(23, "synthetic data", "N")
    a.add_lexunit(24, "labeled data", "N")
    a.add_lexunit(25, "high dimensional", "A")
    a.add_lexunit(26, "negative examples", "N")
    a.add_lexunit(27, "training samples", "N")
    a.add_lexunit(28, "multi-labeled data", "N")
    a.add_lexunit(29, "training instances", "N")
    a.add_lexunit(30, "real-world data", "N")
    a.add_lexunit(31, "data values", "N")
    a.add_lexunit(32, "labeled examples", "N")
    a.add_lexunit(33, "feature vector", "N")
    a.add_lexunit(34, "validation set", "N")
    a.add_lexunit(35, "observed data", "N")
    a.add_lexunit(36, "relational data", "N")
    a.add_lexunit(38, "large data", "N")
    a.add_lexunit(39, "time points", "N")
    a.add_lexunit(40, "feature set", "N")
    a.add_lexunit(41, "sample", "N")
    a.add_lexunit(42, "instances", "N")

    print a


    # ramka Error

    a = XMLCreator(1, 'error')
    a.definition = "This frame describes type of error that coud be used for specific ML algorithm, that solves ML Task. The error value can be given for specific data."
    a.add_fe(1, "Error type", "True", "Type of used error, e.g. mean squared error", [])
    a.add_fe(2, "ML Task", "True", "Machine learning problem", [])
    a.add_fe(3, "Error value", "False", "	The value of mentioned error", [])
    a.add_fe(4, "ML Algorithm", "False", "An algorithm that solves ML Task", [])
    a.add_fe(5, "Dataset", "False", "Type of data for which error is calculated.", [])

    a.add_lexunit(1, "error", "N")
    a.add_lexunit(2, "measure", "N")
    a.add_lexunit(3, "minimize", "V")
    a.add_lexunit(4, "maximize", "V")
    a.add_lexunit(5, "validation set error", "N")
    a.add_lexunit(6, "prediction error", "N")
    a.add_lexunit(7, "expected error", "N")
    a.add_lexunit(8, "error rate", "N")
    a.add_lexunit(9, "error loss", "N")
    a.add_lexunit(10, "generalization error", "N")
    a.add_lexunit(11, "training error", "N")
    a.add_lexunit(12, "approximation error", "N")

    print a

    # ramka Experiment

    a = XMLCreator(1, 'experiment')
    a.definition = "This is a frame for representing relations between ML Experiment and Data used in the experiment, an ML Algorithm / Models applied, measure used to assess the results of an experiment or possibly an Error calculated based on the experiment results, measure or error value  and indication of possible loss or gain in a comparison."
    a.add_fe(1, "ML Experiment", "True", "The ML Experiment.", ["dmop:DM-Experiment", ])
    a.add_fe(2, "Data", "False", "This FE identifies Data used in the ML experiment. ", ["dmop:DM-Data", ])
    a.add_fe(3, "ML Algorithm / Model ", "False", "This FE identifies the particular ML Algorithm/ Model applied during the ML Experiment.", [])
    a.add_fe(4, "Measure", "False", "This FE identifies the Measure used to assess the results of the ML Experiment. ", ["dmop:HypothesisEvaluationMeasure", ])
    a.add_fe(5, "Error", "False", "This FE identifies Error calculated based on the ML Experiment results.", ["dmop:HypothesisEvaluationFunction", ])
    a.add_fe(6, "Measure or error value", "False", "This FE identifies measure or error value calculated during the ML experiment.", [])
    a.add_fe(7, "Loss or gain indication", "False", "This FE indentifies the indication of possible loss or gain in a comparison done during the ML experiment.", [])

    a.add_lexunit(1, "experiment", "N")
    a.add_lexunit(2, "experiment", "V")
    a.add_lexunit(3, "investigation", "N")
    a.add_lexunit(4, "empirical investigation", "N")
    a.add_lexunit(5, "study", "V")
    a.add_lexunit(6, "run", "N")
    a.add_lexunit(7, "run", "V")
    a.add_lexunit(8, "evaluation", "N")

    print a

    # ramka Measure

    a = XMLCreator(1, 'measure')
    a.definition = "This frame represents information about specific measure (and its value) used to estimate performance of specific ML algorithm on some dataset. ML algorithm solves ML task."
    a.add_fe(1, "ML Algorithm / Method", "True", "The ML Algorithm or method that solves a problem.", ["dmop:DM-Algorithm", ])
    a.add_fe(2, "Measure", "True", "A measure that estimate performance of algorithm", [])
    a.add_fe(3, "ML Task", "False", "This FE identifies the particular ML Algorithm/ Model applied during the ML Experiment.", ["dmop:DM-Task", ])
    a.add_fe(4, "Dataset", "False", "A dataset that generates specific value of measure when we used ML algorithm on it.", [])
    a.add_fe(5, "Measure value", "False", "The value of core frame element - measure.", [])
    a.add_fe(6, "measure method", "False", "", [])

    a.add_lexunit(1, "result", "N")
    a.add_lexunit(2, "measure", "V")
    a.add_lexunit(3, "estimate", "N")
    a.add_lexunit(4, "performance", "N")
    a.add_lexunit(5, "better", "V")
    a.add_lexunit(6, "worse", "N")
    a.add_lexunit(7, "precision", "V")
    a.add_lexunit(8, "accuracy", "N")
    a.add_lexunit(9, "lift", "N")
    a.add_lexunit(10, "ROC", "N")
    a.add_lexunit(11, "confusion matrix", "N")
    a.add_lexunit(12, "cost function", "N")
    a.add_lexunit(13, "recall", "N")

    print a

    # ramka Model

    a = XMLCreator(1, 'model')
    a.definition = "This frame represents ML models, identifiees ML Algorithm that produce the models, and model's Characteristics."
    a.add_fe(1, "Model", "True", "The ML Model is a structure with corresponding interpretation that is produced by induction algorithms as a data generalization serving for description or prediction purposes.", ["mop:DM-Model", ])
    a.add_fe(2, "ML Algorithm", "False", "This FE identifies an ML Algorithm that produces the Model. ", [])
    a.add_fe(3, "Characteristic", "False", "This FE identifies the Characteristic of the Model. ", ["dmop:HypthesisCharacteristic", ])

    a.add_lexunit(1, "model", "N")
    a.add_lexunit(2, "hypothesis", "N")
    a.add_lexunit(3, "models", "N")
    a.add_lexunit(4, "hypotheses", "N")
    a.add_lexunit(5, "cluster", "N")
    a.add_lexunit(6, "clustering", "N")
    a.add_lexunit(7, "rules", "N")
    a.add_lexunit(8, "patterns", "N")
    a.add_lexunit(9, "bayes net", "N")
    a.add_lexunit(10, "decision tree", "N")
    a.add_lexunit(11, "graphical model", "N")
    a.add_lexunit(12, "joint distribution", "N")
    a.add_lexunit(13, "neural network", "N")
    a.add_lexunit(14, "generative model", "N")
    a.add_lexunit(15, "bayesian network", "N")

    print a


    # ramka Task

    a = XMLCreator(1, 'task')
    a.definition = "This is a frame for representing ML Task, and optionally an algorithm for solving it."
    a.add_fe(1, "ML Task", "True", "The ML Task.", ["dmop:DM-Task", ])
    a.add_fe(2, "ML Algorithm", "False", "The ML Algorithm", [])

    a.add_lexunit(1, "supervised", "A")
    a.add_lexunit(2, "unsupervised", "A")
    a.add_lexunit(3, "reinforcement learning", "N")
    a.add_lexunit(4, "classification", "N")
    a.add_lexunit(5, "regression", "N")
    a.add_lexunit(6, "clustering", "N")
    a.add_lexunit(7, "density estimation", "N")
    a.add_lexunit(8, "dimensionality reduction", "N")

    print a


    # ramka Problem_solution

    a = XMLCreator(1, 'problem_solution')
    a.definition = "This is a frame for representing relations between ML Task and method that solves it. The solution method could be wider described. The method or collateral problems are probably described in reference article."
    a.add_fe(1, "ML Task", "True", "The ML Task.", ["dmop:DM-Task", ])
    a.add_fe(2, "Solution type", "True", "Method of solving ML Task", [])
    a.add_fe(3, "Solution description", "False", "Extended description of solution for ML Task.", [])
    a.add_fe(4, "Authors/references", "False", "Reference to article that describes solution method.", [])

    a.add_lexunit(1, "solve", "V")
    a.add_lexunit(2, "solving", "V")
    a.add_lexunit(3, "model", "V")
    a.add_lexunit(4, "model", "N")
    a.add_lexunit(5, "assume", "V")

    print a

    """
    text = ""
    text_tab = []
    for filename in os.listdir('downloaded'):
        text_tab.append(open('downloaded/'+filename).read())
        text += open('downloaded/'+filename).read()
        print filename
        print len(text)

    fw = open('results/normal_freq.txt', 'w')
    for w in frequency_words(text):
        fw.write(str(w))
        fw.write('\n')

    fw.close()
    """


    """
    output = open('results/sentences_from_bigram.txt', 'w+')

    a = SentenceFinder('results/bigram.txt', 2, 'downloaded')
    stat_parser = Parser()
    for word in a.words:
        output.write(word)
        output.write('\n')
        print word
        iterator=0
        for s in a.find_sentences(word):
            try:
                with timeout(seconds=10, sentence=s):
                    res_table = getNodes(stat_parser.parse(s), 0, a.active_word)
                    if res_table:
                        output.write(s)
                        output.write('\n')
                        print iterator
                        iterator += 1

            except Exception as e:
                print "timeout"

    output.close()

    """

"""
    with open('input/sentences.txt') as f:
        sentences = [a for a in f.readlines()]

    words = []
    with open('results/bigram.txt') as f:
        for temp in f:
            words.append(eval(temp)[0][0] + " " + eval(temp)[0][1])

    with open('results/normal_freq.txt') as f:
        for temp in f:
            words.append(eval(temp)[0])

    with open('output/sentences.txt', 'w') as out:
        for s in sentences:
            out.write(s)
            out.write('\n')
            for w in words:
                if w in s and len(w) > 2:
                    out.write(w)
                    out.write('\n')

"""


    #print words
    #print sentences





