from __future__ import division, unicode_literals
from collections import Counter
import math

from nltk import word_tokenize, bigrams, FreqDist
from nltk.corpus import stopwords
import os, string
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from textblob import TextBlob as tb

from journals_crawler.utils import StopwordsExtension, tokenize


def frequency_words(text):
    """
    Function creates sorted lists denotes, how often the word appears in corpus.
    :param text:
    :return: list of tuples (word, number)
    """
    print "start"
    print len(text)
    words = [w.strip('!,.?1234567890-=@#$%^&*()_+')for w in unicode(text, errors='ignore').lower().split()]
    counter = Counter(words)

    return remove_stopwords(counter.most_common())
    #print("\n".join("{} {}".format(*p) for p in counter.most_common()))


def tf(word, blob):
    return blob.words.count(word) / len(blob.words)


def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob)


def idf(word, bloblist):

    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))


def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)


def tfidf_frequency(directory, amount):
    text_tab = []

    for filename in os.listdir(directory):
        text_tab.append(tb(unicode(open('downloaded/'+filename).read(), errors='replace').lower()))

    for i, blob in enumerate(text_tab):
        scores = {word: tfidf(word, blob, text_tab) for word in blob.words}
        sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        yield [(i, word, score) for word, score in sorted_words[:amount]]


def bigram_frequency(text, amount):
    tokens = word_tokenize(unicode(text, errors='replace').lower())

    bgs = bigrams(tokens)

    fdist = FreqDist(bgs)
    sorted_bigrams = remove_stopwords_bigram(sorted(fdist.items(), key=lambda x: x[1], reverse=True))
    for k, v in sorted_bigrams[:amount]:
        yield k, v


def remove_stopwords(tab):

    result = [w for w in tab if w[0] not in StopwordsExtension.get_stopwords()]
    return result


def remove_stopwords_bigram(tab):

    result = [w for w in tab if w[0][0] not in StopwordsExtension.get_stopwords() and w[0][1] not in StopwordsExtension.get_stopwords() and len(w[0][0])>2 and len(w[0][1])>2]
    return result


