# coding=UTF-8
import sqlite3

from journals_crawler.utils import remove_stopwords, lematize, chunk


class PreProcessor(object):

    dbname = None

    def __init__(self, dbname):
        self.conn = sqlite3.connect(dbname)
        self.dbname = dbname
        print "Database opened"

        """
        sentence = "Section 3 describes the methodological innovation of Bayesian regularization via information coupling".decode('utf-8')
        lmtzr = WordNetLemmatizer()
        tokens = word_tokenize(sentence)
        tokens_pos = pos_tag(tokens)
        print tokens
        print tokens_pos
        for word, pos in tokens_pos:
            print lmtzr.lemmatize(word, morphy_tag(pos))
        self.chunk(tokens_pos)
        """

    def parsed_sentences(self, table_name):
        cursor = self.conn.execute("SELECT title, path  from " + table_name)
        for row in cursor:

            results = lematize(open(row[1]).read().decode('utf-8'))
            results = remove_stopwords(results)

            for s in results:
                yield chunk(s)

            break  # TODO: remove this

    def __del__(self):
        self.conn.close()
        print "Database closed"
