# coding=UTF-8
import nltk
import sqlite3
from stat_parser import Parser


class StatPreProcessor(object):

    dbname = None

    def __init__(self, dbname):
        self.conn = sqlite3.connect(dbname)
        self.dbname = dbname
        print "Database opened"

    def parsed_sentences(self, table_name):
        cursor = self.conn.execute("SELECT title, path  from "+table_name)
        stat_parser = Parser()
        for row in cursor:
            sentences = nltk.sent_tokenize(open(row[1]).read().decode('utf-8'))
            for s in sentences:
                yield stat_parser.parse(s)
            break

    def __del__(self):
        self.conn.close()
        print "Database closed"
