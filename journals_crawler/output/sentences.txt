The experimental results indicate that probabilistic generative models can achieve competitive multi-label classification performance compared to discriminative methods, and have advantages for datasets with many labels and skewed label frequencies.This paper presents a reciprocal-sigmoid model for pattern classification.

experimental results
multi-label classification
paper presents
generative model
classification performance
results indicate
data
set
model
models
method
methods
results
performance
paper
classification
class
many
dataset
label
sets
present
datasets
labels
result
form
probabilistic
compared
compare
experimental
perform
This paper presents a scheme for evaluating alternative inductive theories based on an objective preference criterion.

paper presents
based
paper
present
alternative
preference
object
objective
base
This paper presents a learning method to identify what information will improve coordination in specific problem-solving situations.

learning method
paper presents
learning
problem
method
paper
information
present
learn
form
specific
improve
Throughout, we give examples where mixed memory models provide a useful representation of complex stochastic processes.This paper presents a method for approximate match of first-order rules with unseen data.

paper presents
data
model
models
use
method
first
paper
example
examples
order
see
process
present
provide
rules
representation
rule
This paper presents a mechanism for giving users a voice by encoding users feedback to topic models as correlations between words into a topic model.

topic models
paper presents
model
models
use
paper
present
topic
users
user
words
First, this paper presents Constrained Conditional Models (CCMs), aframework that augments linear models with declarative constraints as a way to support decisions in an expressive output space while maintaining modularity and tractability of training.

paper presents
linear models
model
models
training
paper
space
work
linear
present
framework
decision
constraints
way
output
main
support
This paper presents a solution which has been guided by psychological and mathematical results.

paper presents
results
paper
present
result
solution
logic
A theory of symbol level learning is sketched, and some reasons are presented for believing that a theory of NKLL will be difficult to obtain.This paper presents a Bayesian method for constructing probabilistic networks from databases.

paper presents
learning
data
two
method
paper
work
present
learn
network
probabilistic
presented
networks
theory
level
obtain
base
In particular, the paper presents an analysis ofdeterminations, a type of relevance knowledge.

paper presents
paper
knowledge
analysis
present
particular
type
This paper presents an algorithm that solves both problems for this domain.

paper presents
algorithm
problem
paper
problems
present
domain
main
It works by successively improving its evaluations of the quality of particular actions at particular states.This paper presents and proves in detail a convergence theorem forQ-learning based on that outlined in Watkins (1989).

paper presents
learning
based
paper
work
state
present
particular
learn
evaluation
theorem
states
convergence
quality
base
actions
This paper presents a fully deployed multistage transfer learning system that has been in continual use for years for thousands of advertising campaigns.

transfer learning
paper presents
learning system
learning
use
paper
present
learn
system
This paper presents an algorithm that generalizes explanation structures and reports empirical results that demonstrate the value of acquiring recursive and iterative concepts.

paper presents
empirical results
algorithm
results
paper
value
general
present
structure
result
concept
rate
empirical
Empirical studies demonstrate that generalizing the structure of explanations helps avoid the recently reported negative effects of learning.This paper presents a new family of decision list induction algorithms based on ideas from the association rule mining context.

paper presents
algorithms based
learning
algorithm
algorithms
based
paper
new
general
present
structure
learn
decision
rate
negative
rule
context
base
idea
This paper presents a methodology that combines the structure of mixed effects models for longitudinal and clustered data with the flexibility of tree-based estimation methods.

paper presents
data
model
models
method
methods
based
paper
present
structure
tree
cluster
estimation
base
This paper presents PCAD, an unsupervised anomaly detection method for large sets of unsynchronized periodic time-series data, that outputs a ranked list of both global and local anomalies.

paper presents
anomaly detection
data
set
method
time
paper
large
sets
present
output
rank
local
detection
The paper presents the methodology for achieving that.

paper presents
method
paper
present
This paper presents a novel empirical analysis that helps to understand this variation.

paper presents
paper
analysis
present
empirical
novel
This paper presents a new approach for learning multiagent coordination strategies that addresses these issues.

paper presents
new approach
learning
approach
paper
new
present
learn
rate
This paper presents a provably correct algorithm for inferring any minimum-state deterministic finite state automata (FSA) from a complete ordered sample using limited total storage and without storing example strings.

paper presents
algorithm
using
paper
example
order
state
present
sample
without
let
finite
total
This paper presents four novel continuous feature selection approaches directly minimising the classifier performance.

feature selection
paper presents
approach
performance
paper
feature
class
approaches
present
selection
classifier
form
directly
perform
novel
This paper presents a solution based on the use of reasonable policies to provide guidance.

paper presents
use
based
paper
present
provide
solution
able
base
However, the development of appropriate feature extraction methods is a tedious effort, particularly because every new classification task requires tailoring the feature set anew.This paper presents a unifying framework for feature extraction from value series.

paper presents
feature set
set
use
method
methods
paper
new
value
classification
work
feature
class
task
present
particular
framework
every
requires
This paper presents a multi-objective optimization approach for the alternative clustering problem based on evolutionary algorithms, which generates a collection of pareto-optimal clustering solutions.

alternative clustering
paper presents
clustering solutions
clustering solution
algorithm
problem
algorithms
approach
based
paper
clustering
present
optimal
solution
optimization
rate
cluster
alternative
solutions
object
objective
base
We present a case-based reasoning system called TempoExpress for addressing this problem, and describe the experimental results obtained with our approach.This paper presents a cooperative evolutionary approach for the problem of instance selection for instance based learning.

experimental results
paper presents
results obtained
learning
problem
approach
results
based
paper
case
present
instance
selection
learn
result
system
obtained
called
experimental
describe
obtain
base
We show that a combination of cooperative coevolution together with the principle of divide-and-conquer can be very effective both in terms of improving performance and in reducing computational cost.This paper presents a method for learninggraded concepts.

computational cost
paper presents
learning
method
performance
paper
show
present
learn
terms
form
cost
computational
concept
perform
The conference paper presented only intuition driven guidelines on how to build robust predictive models, this paper presents a theoretically optimal least squares solution.

paper presents
predictive models
model
models
paper
present
optimal
solution
presented
theoretical
least
http://www.kamishima.net/sushi/.This paper presents a model-based, unsupervised algorithm for recovering word boundaries in a natural-language text from which they have been deleted.

paper presents
algorithm
model
based
paper
present
bound
language
let
natural
base
This paper presents some results on the probabilistic analysis of learning, illustrating the applicability of these results to settings such as connectionist networks.

paper presents
learning
set
two
results
paper
work
analysis
present
learn
result
setting
network
probabilistic
networks
This paper presents a description and empirical evaluation of a new induction system.

paper presents
empirical evaluation
paper
new
present
system
evaluation
empirical
Visualizations of experimental results also indicate our embedding space preserves the local smooth manifold structures existing in real-world data.This paper presents an integration of induction and abduction in INTHELEX, a prototypical incremental learning system.

experimental results
paper presents
learning system
real-world data
learning
data
also
results
paper
space
present
structure
learn
result
system
experimental
real
existing
local
This paper presents a method for approximating posterior distributions over the parameters of a given PRISM program.

posterior distribution
paper presents
method
given
paper
distribution
parameters
parameter
present
distributions
This paper presents an exact technique for finding all these mixture components for small scale problems and considers an approximate method for cases where the exact approach is infeasible.The paper is organised as follows.

paper presents
one
problem
approach
method
paper
case
problems
consider
present
small
find
cases
Animated figures, links to datasets, and further resources are available at http://www.maunz.de/mlj-res.This paper presents a representation for melodic segment classes and applies it to music data mining.

data mining
paper presents
data
set
paper
class
dataset
sets
present
datasets
available
representation
classes
able
We find that both approaches can substantially increase the prediction accuracy.This paper presents a method of vision-based reinforcement learning by which a robot learns to shoot a ball into a goal.

reinforcement learning
paper presents
prediction accuracy
learning
approach
method
based
paper
approaches
accuracy
present
learn
prediction
find
goal
base
coli sequences.This paper presents a new method that deals with a supervised learning task usually known as multiple regression.

supervised learning
paper presents
learning task
learning
method
paper
new
task
present
regression
learn
multiple
known
sequence
This paper presents a novel large-scale feature selection algorithm that is based on variance analysis.

feature selection
paper presents
algorithm
based
paper
feature
large
analysis
present
selection
variance
base
novel
1995) and MapReduce(Dean and Ghemawat 2010), are proposed to facilitate programming on high-performance distributed computing infrastructures to handle very large-scale problems.This paper presents a novel distributed parallel algorithm for handling large-scale problems in feature selection.

feature selection
paper presents
algorithm
problem
performance
paper
proposed
feature
problems
large
present
structure
selection
form
high
propose
perform
hand
novel
In the experiment, there is one worker on each node, and each worker runs with 8 threads.This paper presents a distributed parallel feature selection algorithm based on maximum variance preservation.

feature selection
paper presents
algorithm based
algorithm
one
based
paper
work
feature
present
selection
node
maximum
variance
base
