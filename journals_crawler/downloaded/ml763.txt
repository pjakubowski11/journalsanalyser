
      We introduce graphical learning algorithms and use them to produce bounds on error deviance for unstable learning algorithms which possess a partial form of stability. As an application we obtain error deviance bounds for support vector machines (SVMs) with variable offset parameter.
      