We consider a variant of the multi-armed bandit model, which we call scratch games, where the sequences of rewards are finite and drawn in advance with unknown starting dates. This new problem is motivated by online advertising applications where the number of ad displays is fixed according to a contract between the advertiser and the publisher, and where a new ad may appear at any time. The drawn-in-advance assumption is natural for the adversarial approach where an oblivious adversary is supposed to choose the reward sequences in advance. For the stochastic setting, it is functionally equivalent to an urn where draws are performed without replacement. The non-replacement assumption is suited to the sequential design of non-reproducible experiments, which is often the case in real world. By adapting the standard multi-armed bandit algorithms to take advantage of this setting, we propose three new algorithms: the first one is designed for adversarial rewards; the second one assumes a stochastic urn model; and the last one is based on a Bayesian approach. For the adversarial and stochastic approaches, we provide upper bounds of the regret which compare favorably with the ones of Exp3 and UCB1. We also confirm experimentally that these algorithms compare favorably with Exp3, UCB1 and Thompson Sampling by simulation with synthetic models and ad-serving data.In its most basic formulation, the multi-armed bandit problem can be stated as follows: there are K arms, each having an unknown, and infinite sequence of bounded rewards. At each step, a player chooses an arm and receives a reward issued from the corresponding sequence of rewards. The player needs to explore the arms to find profitable actions, but on other hand the player would like to exploit as soon as possible the best arm identified. Which policy should the player adopt in order to minimize the regret against the best arm?The stochastic formulation of this problem assumes that each arm delivers rewards that are independently drawn from an unknown distribution. Efficient solutions based on optimism in the face of uncertainty have been proposed for this setting (Lai and Robbins 1985; Agrawal 1995). They compute an upper confidence index for each arm and choose the arm with the highest index. In this case, it can be shown that the regret, the cumulative difference between the optimal reward and the expectation of reward, is bounded by a logarithmic function of time, which is the best possible. Subsequent work introduced simpler policies, proven to achieve logarithmic bound uniformly over time (Auer et al. 2002a). Recently, different variants of these policies have been proposed, to take into account the observed variance (Audibert et al. 2009), based on Kullback-Leibler divergence (Garivier and Cappé 2011), the tree structure of arms (Kocsis and Szeoesvàri 2006; Bubeck et al. 2008), or the dependence between arms (Pandey et al. 2007).Another approach to solve the multi-armed bandit problem is to use a randomized algorithm. The Thompson Sampling algorithm, one of the oldest multi-armed bandit algorithm (Thompson 1933), is based on a Bayesian approach. At each step an arm is drawn according to its probability of being optimal. The observation of the reward updates this probability. Recent papers have shown its accuracy on real problems (Chapelle and Li 2011), and that it achieves logarithmic expected regret (Agrawal and Goyal 2012), and that it is asymptotically optimal (Kaufman et al. 2012b).There are however several applications, including online advertising, where the rewards are far from being stationary random sequences. A solution to cope with non-stationarity is to drop the stochastic reward assumption and assume the reward sequences to be chosen by an adversary. Even with this adversarial formulation of the multi-armed bandit problem, a randomized strategy like Exp3 provides the guarantee of a minimal regret (Auer et al. 2002b; Cesa-Bianchi and Lugosi 2006).Another usual assumption which does not fit well the reality of online advertising is the unlimited access to actions. Indeed, an ad server must control the ad displays in order to respect the advertiser’s budgets, or specific requirements like “this ads has to be displayed only on Saturday”. To model the limited access to actions, sleeping bandits have been proposed (Kleinberg and Niculescu-Mizil Sharma 2008). At each time step, a set of available actions is drawn according to an unknown probability distribution or selected by an adversary. The player then observes the set, and plays an available action. This setting was later completed to take into account adversarial rewards (Kanade et al. 2009). Another way to model the limited access to actions is to consider that each arm has a finite lifetime. In this mortal bandits setting, each appearing or disappearing arm changes the set of available actions. Several algorithms were proposed and analyzed by Chakrabarti et al. (2008) for mortal bandits under stochastic reward assumptions.In this paper, we propose a variant of mortal and sleeping bandits, which we call scratch games, where the sequences of rewards are finite and drawn in advance with known lengths, and with unknown starting dates. We assume the sequences lengths to be known in advance: indeed, the maximum display counts are usually fixed in advance by a contract between the advertiser and the publisher. This knowledge makes our setting different from sleeping bandits where the sequences of reward are infinite. The ad serving optimization is a continuous process which has to cope with appearing and disappearing ads along the way. During a long period of time, it is not possible to know in advance the number of ads that the ad server has to display, since it depends of new contracts. To fit this application constraints, for the scratch games setting, the starting dates of new scratch games and the maximum number of scratch games are unknown to the player. This point differs from mortal bandits.We consider both an adversarial reward setting where the sequences are determined by an oblivious adversary and a stochastic setting where each sequence is assumed to be drawn without replacement from a finite urn. These two settings extends and complete the work of Chakrabarti et al. (2008). The non-replacement assumption is better suited to the sequential design of non-reproducible experiments. This is the case for telemarketing where each targeted client is only reachable once for each campaign. This is also the case for targeted online advertising when the number of display of a banner to an individual is limited. This limit, called capping, leads to an urn model (formally when the capping is set to one).the first one (in Sect. 3), E3FAS, is a randomized algorithm based on a deterministic assumption: an adversary has chosen the sequences of rewards,the second one (in Sect. 4), UCBWR, is a deterministic algorithm based on a stochastic assumption: the sequences of rewards are drawn without replacement according to unknown distributions,the last one (in Sect. 5), TSWR, is a randomized algorithm based on a Bayesian assumption: the mean reward of each scratch games is distributed according to a beta-binomial law.For the first two, we will provide regret bounds for scratch games, which compare favorably to the UCB1 and Exp3 bounds. In Sect. 6, we will test these policies on synthetic problems to study their behavior with respect to different factors coming from application constraints. We will complete these tests with a realistic ad serving simulation.We consider a set of K scratch games. Each game i has a finite number of tickets N
                      
                        i
                       including M
                      
                        i
                       winning tickets, and a starting date t
                      
                        i
                      . Let x
                      
                        i,j
                       be the reward of the j-th ticket for game i. We assume that the reward of each ticket is bounded: 0≤x
                      
                        i,j
                      ≤1. A winning ticket is defined as a ticket which has a reward greater than zero.The number of tickets N
                      
                        i
                       of current games are known to the player.The number of winning tickets M
                      
                        i
                      , the starting dates t
                      
                        i
                       of new scratch games, the total number of scratch games K, and the sequence of reward x
                      
                        i,j
                       of each game are unknown to the player.At each time step t, the player chooses a scratch game i in the set of current scratch games [K
                      
                        t
                      ], and receive the reward \(x_{i,n_{i}(t)}\), where n
                      
                        i
                      (t) is the number of scratched tickets at time t of the game i.The scratch game problem is different from the multi-armed bandit problem. Indeed, in the multi-armed bandit setting, to maximize his gain the player has to find the best arm as soon as possible, and then exploit it. In the scratch game setting, the number of tickets is finite. When the player has found the best game, he knows that this game will expire at a given date. The player needs to re-explore before the best game finishes in order to find the next best game. Moreover, a new best game may appear. The usual tradeoff between exploration and exploitation has to be revisited. In the next sections, we will detail respectively adversarial, stochastic, and stochastic Bayesian declinations for the scratch games of the well known algorithms Exp3 (Auer et al. 2002b), UCB1 (Auer et al. 2002a), and TS (Thompson 1933) used for the multi-armed bandits.
                        Exp3 (Exponential weight algorithm for Exploration and Exploitation) is a powerful and popular algorithm for adversarial bandits (Auer et al. 2002b). The Achilles heel of this algorithm is its sensitivity to the exploration parameter γ: a value too low or too high for this parameter leads to a bad trade-off between exploitation and exploration. The intuition used to adapt Exp3 to scratch games is the following: when a new game starts the exploration term γ has to increase in order to explore it, and when a game ends, the number of games decreases and the exploration term γ has to decrease.Let T
                        
                          m
                         be the time when a game starts or ends, T
                        
                          m+1 be the time when another game starts or ends, K
                        
                          m
                         be the number of games during the time period [T
                        
                          m
                        ,T
                        
                          m+1[, \(\varDelta_{m}=G_{T_{m+1}}-G_{T_{m}}\) be the gain between times T
                        
                          m
                         and T
                        
                          m+1 and \(\varDelta ^{*}_{m}=G^{*}_{T_{m+1}}-G^{*}_{T_{m}}\) be the optimal gain between times T
                        
                          m
                         and T
                        
                          m+1. We first give an upper bound for the expected regret obtained by E3FAS for a given period and a given γ
                        
                          m
                        .If the optimal gain \(\varDelta^{*}_{m}\) is known, we can use this bound to evaluate the value of γ
                        
                          m
                         each time a game ends or starts. We use \(\gamma_{m}^{*}\) to denote the value of the parameter γ
                        
                          m
                         which optimizes the upper bound given by Theorem 1.The proofs of Theorem 1 and Corollaries 1.1 and 1.2 can be found in the Appendix. The obtained bound (the first inequality) is less or equal to the one of Exp3 for the scratch games (the second inequality). Notice that these upper bounds of the weak regret for scratch games are theoretical: Exp3 requires the knowledge of \(G_{T}^{*}\) and K, while E3FAS requires the knowledge of a the number of time periods L, the numbers of scratch games K
                        
                          m
                        , and the values of \(\varDelta_{m}^{*}\).In real application, the order of magnitude of the mean reward of all games μ is usually known. For example, μ≈1/1000 for the click-through-rate on banners, μ≈1/100 for emailing campaigns, and μ≈5/100 for telemarketing. In these cases, it is reasonable to assume that \(G^{*}_{T} \approx\mu T \ll T\). If this prior knowledge is not available, it is possible to use T to bound \(G_{T}^{*}\).Where G
                        
                          t
                        (i) is the cumulated reward of game i. The expectation is taken over the sequences of draws \(x_{i_{1}} (1), x_{i_{2}} (2),\ldots, x_{i_{t}} (t)\) according to the probability law of each game i. Knowing the number of tickets N
                        
                          i
                        , the starting date t
                        
                          i
                         of each current scratch game i, we would like to find an efficient policy in order to optimize uniformly the expected gain E[G
                        
                          t
                        (π)] over all the sequences of draws.With UCBWR policy, the mean reward is balanced with a confidence interval weighted by one minus the sampling rate of the game. Then, when the number of plays n
                        
                          i
                        (t) of the scratch game i increases, the confidence interval term decreases faster than with UCB1 policy. The exploration term tends to zero when the sampling rate tends to 1. The decrease of the exploration term is justified by the fact that the potential reward decreases as the sampling rate increases. Notice that if all games have the same sampling rates, the rankings provided by UCB1 and UCBWR are the same. The difference between rankings provided by UCBWR and UCB1 increases as the dispersion of sampling rates increases. We can expect different performances, when the initial numbers of tickets N
                        
                          i
                         are different. In this case, scratching a ticket from a game with a low number of tickets has more impact on its upper bound than from a game with a high number of tickets.equal at the initialization when the expected sampling rate is zero,and lower when the expected sampling rate increases.The proofs of Theorem 2 and Corollary 2.1 can be found in the Appendix.The Thompson sampling algorithm is a generic algorithm which can be applied with various priors. In the case of a Bernoulli distribution of rewards, recent papers have shown that it achieves logarithmic expected regret (Agrawal and Goyal 2012) and that it is asymptotically optimal (Kaufman et al. 2012b). We propose to use the Thompson sampling algorithm to the scratch games using a beta-binomial law to model the parameters likelihood rather than the beta law used for the multi-armed bandits (Chapelle and Li 2011).To speed up the convergence of the algorithm, we can use the value of the mean reward of all games μ, which is often known in real application, for initializing the values of m
                        
                          i
                        , n
                        
                          i
                        , and μ
                        
                          i
                         of each game i: we choose the initial value μ
                        
                          i
                        =μ for having a prior in the order of magnitude of its true value, and we choose m
                        
                          i
                        =1, and n
                        
                          i
                        =1/μ
                        
                          i
                         in order to begin with a large variance around this initial prior. For example, the click-through rate on a web-portal is approximatively of 1/1000. In this case, we will use to initialize the game i: μ
                        
                          i
                        =1/1000, m
                        
                          i
                        =1, n
                        
                          i
                        =1000. When μ is unknown, μ
                        
                          i
                         can be initialized to any value lesser than 1, with m
                        
                          i
                        =0 and n
                        
                          i
                        =0.We do not provide bounds of the regret for Thomson Sampling Without Replacement, and to the best of our knowledge, it is an open problem.Due to the budget constraints, the scratch games have finite sequences of rewards.Due to the continuous optimization, the scratch games have different and unknown starting dates.Due to the competition between ads on a same target (cookies, profiles …), to the relevance of the ad with page content, which can change, and to unknown external factors, the mean reward of a scratch game can change over time.We have chosen a Pareto distribution to draw the number of tickets of 100 scratch games, with parameters x
                        
                          m
                        =200 and k=1. This choice is driven by the concern to be as close as possible to our application: a lot of small sequences and a small number of very large sequences. The number of winning tickets of each scratch game is drawn according to a Bernoulli distribution, with parameter p
                        
                          i
                         drawn from a uniform distribution between 0 and 0.25. 210314 tickets including 33688 winning tickets spread over 100 scratch games are drawn. For each trial and for each scratch game i, a sequence of rewards is drawn according to the urn model parametrized by the number of winning tickets m
                        
                          i
                         and the number of tickets n
                        
                          i
                        .for games with even index, during the time period [0,N/2] the probability of reward is multiplied by two, and during the time period [N/2,N] the probability of reward is divided by two.otherwise, during the time period [0,N/2] the probability of reward is divided by two, and during the time period [N/2,N] the probability of reward is multiplied by two.In these illustrative synthetic problems, we observe that if its prior holds, the Thompson Sampling algorithm Without Replacement is the best, and for the tested non-stationary distributions of rewards it is one of the worst. UCBWR is an efficient and robust algorithm which obtains good performances on the three synthetic problems. E3FAS also exhibits good performances, and it provides more guarantees on non stationary data. Finally, we observe that algorithms designed for scratch games outperform those designed for multi-armed bandits: for adversarial approach E3FAS outperforms Exp3, for stochastic approach UCBWR outperforms UCB1, and for Bayesian approach TSWR outperforms TS. In the next section we will test the same algorithms on complex real data.In the pay-per-click model, the ad server displays different ads on different contexts (profiles × web pages) in order to maximize the click-through rate. To evaluate the impact of E3FAS, Exp3, UCBWR, UCB1, TS and TSWR on the ad server optimization, we have simulated it.Each ad is considered as a scratch game, with a finite number of tickets corresponding to the number of displays, including a finite number of winning tickets corresponding to the clicks, and with a sequence of rewards corresponding to sequences of ad displays and clicks. When a scratch game is selected by a policy, the reward is read from its sequence of rewards. In the simulation, we consider that the observed displays of each ad represent their total inventories on all web pages. The goal of the optimization is then to increase the number of displays of the ads with a high click-through rate and to decrease the number of displays of the others.In this experiment, we have considered a simple context: the optimization of a single page for all profiles of cookies during one week. The optimization over a long period of time of many pages for several profiles of cookies will generate many scratch games. As the number of scratch games K increases, the difference between the regret bounds of Exp3 and the one of E3FAS increases (see Corollary 1.2). We can expect more significant differences of gains between Exp3 and E3FAS. Notice that for an optimization over a long period of time, such as several months, to tune the exploration factor of E3FAS, we have to consider a sliding time horizon T as a parameter of the optimization, that we have to tune. UCBWR does not suffer from this drawback.We have proposed a new problem to take into account finite sequences of rewards drawn in advance with unknown starting dates: the scratch games. This problem corresponds to applications where the budget is limited, and where an action cannot be repeated identically, which is often the case in real world. We have proposed three new versions of well known algorithms, which take advantage of this problem setup. In our experiments on synthetic problems and on real data, the three proposed algorithms outperformed those designed for multi-armed bandits. For E3FAS and UCBWR, we have shown that the proposed upper bounds of the weak regret are less or equal respectively to those of Exp3 and of UCB1. For TSWR the upper bound of the weak regret is an open problem. Between the three policies proposed for this problem, our experiments lead us to conclude that TSWR is the best when its prior holds, and E3FAS is the best for complex distributions of rewards, which correspond to the data collected on our ad server. This preliminary work on scratch games has shown its interest for online advertising. In a future work, we will consider extensions of this setting to better fit application constraints. Indeed, due to the information system and application constraints, there is a delay between the choices of the game and the reception of rewards. Moreover, in this work we have considered the optimization of ads on a single page. To optimize the ad server policy, we need to optimize the ad displays on many pages having a structure of dependence.The demonstration of Theorem 1 uses the mathematical framework provided by Auer et al. (2002b) for Exp3. The main difference with Exp3 is that we consider scratch games which have unknown starting and ending dates. We circumvent this problem by considering the expected regret during a time period [T
                          
                            m
                          ,T
                          
                            m+1], where the number of games K
                          
                            m
                           is constant, rather than for an horizon T. This implies some changes in the demonstration to take into account the initialization of weights at time T
                          
                            m
                           (see Eq. (3)), the way to obtain a lower bound of the logarithm of the weights (see Eq. (5)), the gain of the optimal policy (see Eq. (9)).The first step of the proof consists in upper bounding the difference between the logarithms of the sum of the weights at time T
                                
                                  m+1+1 and at time T
                                
                                  m
                                . Using some algebraic arguments and definitions coming from the E3FAS algorithm, this is achieved at Eq. (2).In the second step, using the fact that the logarithm of the sum of weights is higher than the logarithm of a given weight, we provide a lower bound of this quantity (see Eq. (5)).In the last step, combining the lower bound and the upper bound, we obtain an inequality (see Eq. (6)). Taking the expectation of both size of this inequality, using some algebraic statements, we provide the proof of Theorem 1.Let be \(f(\gamma_{m})=(e-1)\gamma_{m} \varDelta^{*}_{m} +\frac{K_{m}\ln K_{m}}{\gamma _{m}}\). Then, solving f′(γ
                          
                            m
                          )=0, we provide the proof of Corollary 1.1.  □The proof of this theorem uses the mathematical framework provided for UCB1 by Auer et al. (2002a). The concentration inequality used, the Serfling inequality, differs from the one use in the demonstration UCB. Hence, the bounding of n
                          
                            i
                          (t) is different for UCBWR (see Eq. (14)). In the case of scratch games, the optimal static policy plays successively all the games rather than the best one for multi-armed bandits. It impacts the value of Δ
                          
                            i
                          (t). However, since Δ
                          
                            i
                          (t) still deterministic, it does not change the proof.The first step of the proof consists in bounding n
                                
                                  i
                                (k), when i is a suboptimal game and when \(\hat{\mu}_{i}(k)\) and \(\hat{\mu }_{i_{k}^{*}}(k)\) are in their confidence interval (see Eq. (14)).In the second step, we bound n
                                
                                  i
                                (t) using the fact that n
                                
                                  i
                                (t) is an increasing random variable (see Eq. (15)).The two bounds are not consistent. We conclude that \(\hat{\mu }_{i}(k)\) or \(\hat{\mu}_{i_{k}^{*}}(k)\) are not in their confidence interval. Using the Serfling inequality we bound this probability by k
                                −4 (see Eq. (16)).In the last step, we take the expectation of the bound of n
                                
                                  i
                                (t) (Eq. (15)) for all sequence of draws, and we bound the probability that at step k, \(\hat{\mu}_{i}(k)\) or \(\hat{\mu }_{i_{k}^{*}}(k)\) are not in their confidence interval by the sum of these probabilities to obtain the bound of E[n
                                
                                  i
                                (t)].By multiplying the previous inequality by Δ(t), and by summing all the suboptimal scratch games, we provide the proof of the Corollary 2.1.  □